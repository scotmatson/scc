#!/bin/sh

if [ "$1" ]
then
    sed -i '' 1d $1
    sed -i '' -e 's/^"//' -e 's/"$//' $1
else
	echo "usage: scc source"
fi
